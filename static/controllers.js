var testApp = angular.module('testApp', ['ngRoute']);

testApp.controller('MainController', function($scope, $route, $routeParams, $location,$http) {
     $scope.$location = $location;
     $scope.$routeParams = $routeParams;

 });

 testApp.controller('BookController', function($scope, $routeParams,$rootScope) {
     $scope.bookId = $routeParams.bookId;
     $scope.currentBook = $rootScope.books.books[$scope.bookId-1];
     console.log($scope.currentBook);
 });

 testApp.controller('AuthorController', function($scope, $routeParams,$rootScope) {
     $scope.menu = {
      opened : false
     };
     $scope.authorId = $routeParams.authorId;
     $scope.currentAuthor= $rootScope.authors.authors[$scope.authorId-1];
 });

 testApp.controller('GenreController', function($scope, $routeParams,$rootScope) {
     $scope.genre = $routeParams.genre;
     console.log($rootScope.genres.genres.length);
     for(var i = 0; i < $rootScope.genres.genres.length; i++) {
      console.log($rootScope.genres.genres[i].Name + " " + $scope.genre);
        if($rootScope.genres.genres[i].Name == $scope.genre) {
          $scope.currentGenre = $rootScope.genres.genres[i] ;
   }
}
     
 });

testApp.config(function($routeProvider, $locationProvider) {
  $routeProvider
   .when('/Book/:bookId', {
    templateUrl: '/static/views/book.html',
    controller: 'BookController',
    resolve: {
      delay: function($rootScope,$http,$routeParams) {
        console.log($routeParams);
      }
    }
  })
  .when('/Author/:authorId', {
    templateUrl: '/static/views/author.html',
    controller: 'AuthorController'
  })

   .when('/Genre/:genre', {
    templateUrl: '/static/views/genres.html',
    controller: 'GenreController',
    resolve: {
      delay: function($rootScope,$http) {
        $http.get('/static/genres.json').success(function(data) {
        $rootScope.genres = data;
    });
  }
}
})
   .when('/books', {
    templateUrl: '/static/views/books.html',
    controller: 'BookController',
    resolve: {
      delay: function($rootScope,$http) {
          $http.get('/static/books.json').success(function(data) {
          $rootScope.books = data;
         });
          $http.get('/static/authors.json').success(function(data) {
          $rootScope.authors = data;
        });

  }
}
})
   .when('/authors', {
    templateUrl: '/static/views/authors.html',
    controller: 'AuthorController',
    resolve: {
      delay: function($rootScope,$http) {
        $http.get('/static/books.json').success(function(data) {
          $rootScope.books = data;
         });
          $http.get('/static/authors.json').success(function(data) {
          $rootScope.authors = data;
        });
    }
  }
  }).
      otherwise({
        redirectTo: '/'
      });;
});